//
//  DataServiceAPI.h
//  ContactsApp
//
//  Created by Leonardo Amigoni on 2/13/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

// THIS IS THE SINGLETON API THAT WILL BE REFERENCE FROM THE WHOLE APP AND INTERFACE WITH THE SERVER API. IT WILL PROVIDE A SINGLE POINT OF CONNECTION. IF IT'S EVER NECESSARY TO CONFIGURE A NEW BACKEND IT CAN BE DONE FROM THIS FILE
#import <Foundation/Foundation.h>

@interface DataServiceAPI : NSObject

@property (strong, nonatomic) NSArray *contacts;
@property NSString *uid; //User ID of the registered user

+(DataServiceAPI*) sharedManager;

- (void) registerUserWithEmail: (NSString *)email password:(NSString *)password completion:(void(^)(BOOL finished))completion;

- (void) loginUserWithEmail: (NSString *)email password:(NSString *)password completion:(void(^)(BOOL finished))completion;

@end
