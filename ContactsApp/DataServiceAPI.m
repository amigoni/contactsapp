//
//  DataServiceAPI.m
//  ContactsApp
//
//  Created by Leonardo Amigoni on 2/13/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

// THIS IS THE SINGLETON API THAT WILL BE REFERENCE FROM THE WHOLE APP AND INTERFACE WITH THE SERVER API. IT WILL PROVIDE A SINGLE POINT OF CONNECTION. IF IT'S EVER NECESSARY TO CONFIGURE A NEW BACKEND IT CAN BE DONE FROM THIS FILE

#import "DataServiceAPI.h"

@implementation DataServiceAPI

+ (DataServiceAPI*)sharedManager
{
    //Ensuring Singleton
    static DataServiceAPI *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[DataServiceAPI alloc] init];
    });
    return _sharedInstance;
}


- (id)init {
    if (self = [super init]) {
        //IF the user is register get data from "Server"
        self.uid = [[NSUserDefaults standardUserDefaults] valueForKey:@"uid"];
        if(_uid){
            [self loadDataFromFile:@"ContactsData.json"];
        } else {
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"loggedIn"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    
    return self;
}

-(void) loadDataFromFile:(NSString*)fileLocation{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:[fileLocation stringByDeletingPathExtension] ofType:[fileLocation pathExtension]];
    NSData* data = [NSData dataWithContentsOfFile:filePath];
    
    NSError* error;
    NSArray* rawContacts = [NSJSONSerialization JSONObjectWithData:data
                                                         options:kNilOptions
                                                           error:&error];
    
    
    
    //Sort contacts by NAME. The incoming data has only NAME for now idealy we would split
    //this in FIRST and LAST and add different entries to the dictionary for both
    //to be able to sort either by first or lastname
    NSSortDescriptor *nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObject:nameDescriptor];
    self.contacts = [rawContacts sortedArrayUsingDescriptors:sortDescriptors];
    
    /*
    NSMutableArray * parsedContacts = [[NSMutableArray alloc] init];
    
    for (NSDictionary *contact in json) {
        if (contact[@"email"]) {
            <#statements#>
        }
    }
    */
}

- (void) registerUserWithEmail: (NSString *)email password:(NSString *)password completion:(void(^)(BOOL finished))completion {
    
    NSString * uid = @"DummyUID";//This uid is supposed to be returned by server after registration
    [[NSUserDefaults standardUserDefaults] setValue:uid forKey:@"uid"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"loggedIn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self loadDataFromFile:@"ContactsData.json"];
    
    completion(YES);
}

- (void) loginUserWithEmail: (NSString *)email password:(NSString *)password completion:(void(^)(BOOL finished))completion {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"loggedIn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    completion(YES);
}

@end
