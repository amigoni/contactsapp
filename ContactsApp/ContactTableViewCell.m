//
//  ContactTableViewCell.m
//  ContactsApp
//
//  Created by Leonardo Amigoni on 2/13/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "ContactTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ContactTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.contactDataCopy = [[NSDictionary alloc] init];
    self.iconView.layer.cornerRadius = 5;
    self.iconView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void) configureCell: (NSDictionary *)contactData {
    self.contactDataCopy = contactData;
    self.topLabel.text = contactData[@"name"];
    [self.iconView sd_setImageWithURL:[NSURL URLWithString:contactData[@"imageUrl"]]
                      placeholderImage:[UIImage imageNamed:@"PlaceHolderSmall.png"]];
}

@end
