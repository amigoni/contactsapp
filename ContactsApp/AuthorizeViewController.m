//
//  AuthorizeViewController.m
//  nowApp
//
//  Created by Leonardo Amigoni on 1/24/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "AuthorizeViewController.h"
#import "DataServiceAPI.h"
#import "ContactListTableViewController.h"

@interface AuthorizeViewController ()

@end


@implementation AuthorizeViewController{
    DataServiceAPI *dataServiceAPI;
    BOOL isSignUpState;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    dataServiceAPI = [DataServiceAPI sharedManager];
    isSignUpState = YES;
    
    self.signUpButton.layer.cornerRadius = 10;
    self.signUpButton.clipsToBounds = YES;
}


- (void) openContactList {
    // user is logged in, check authData for data
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ContactListTableViewController *sfvc = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:sfvc animated:NO completion:nil];
}



#pragma mark - Button Actions

- (IBAction)onSignUpPressed:(id)sender {
    //TODO: Verify Both Email and Password Validity before communicating with server
    
    if (isSignUpState) {
        [dataServiceAPI registerUserWithEmail:self.emailTextField.text password:self.passwordTextField.text completion:^(BOOL finished) {
            [self openContactList];
        }];
    }
    else {
        [dataServiceAPI loginUserWithEmail:self.emailTextField.text password:self.passwordTextField.text completion:^(BOOL finished) {
            [self openContactList];
        }];
    }
}


- (IBAction)onLoginSignupSwitchPressed:(id)sender {
    if (isSignUpState == YES) {
        isSignUpState = NO;
        _titleLabel.text = @"Login";
        [_instructionsButton setTitle:@"Forgot your password? Click here" forState:UIControlStateNormal];
        [_instructionsButton setEnabled:YES];
        
        [_signUpButton setTitle:@"Login" forState:UIControlStateNormal];
        
        [_signUpLoginSwitchButton setTitle:@"Don't have an account? Click here" forState:UIControlStateNormal];
        
    } else {
        isSignUpState = YES;
        _titleLabel.text = @"Create an account";
        [_instructionsButton setTitle:@"min 8 characters" forState:UIControlStateNormal];
        [_instructionsButton setEnabled:NO];
        
        [_signUpButton setTitle:@"Signup" forState:UIControlStateNormal];
        
        [_signUpLoginSwitchButton setTitle:@"Already have an account? Click here" forState:UIControlStateNormal];
    }
}


@end
