//
//  ContactListTableViewController.h
//  ContactsApp
//
//  Created by Leonardo Amigoni on 2/13/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactListTableViewController : UITableViewController <UISearchBarDelegate,UISearchControllerDelegate,UISearchResultsUpdating>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideBarButton;

@end
