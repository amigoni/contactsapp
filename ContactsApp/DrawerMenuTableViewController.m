//
//  DrawerMenuTableViewController.m
//  ContactsApp
//
//  Created by Leonardo Amigoni on 2/13/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "DrawerMenuTableViewController.h"
#import "AuthorizeViewController.h"

@interface DrawerMenuTableViewController ()

@end

@implementation DrawerMenuTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(BOOL)prefersStatusBarHidden{
    return NO;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"loggedIn"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AuthorizeViewController *sfvc = [storyboard instantiateViewControllerWithIdentifier:@"AuthorizeViewController"];
        [self presentViewController:sfvc animated:NO completion:nil];
    }
}



#pragma mark - Navigation


@end
