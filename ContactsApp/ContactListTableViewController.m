//
//  ContactListTableViewController.m
//  ContactsApp
//
//  Created by Leonardo Amigoni on 2/13/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "ContactListTableViewController.h"
#import "ContactDetailTableViewController.h"
#import "ContactTableViewCell.h"
#import "DataServiceAPI.h"
#import "SWRevealViewController.h"


@interface ContactListTableViewController ()

@property (strong, nonatomic) UISearchController *searchController;

@end

@implementation ContactListTableViewController{
    DataServiceAPI *dataServiceApi;
    NSDictionary *selectedContactData;
    NSArray *tableData;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Contacts";
    
    //Setup for Menu Drawer Button and Control
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sideBarButton setTarget: self.revealViewController];
        [self.sideBarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    //Setup the Search Controller
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.searchController.searchBar.translucent = false;
    self.searchController.searchBar.placeholder = @"enter a name";
    self.searchController.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    self.searchController.hidesNavigationBarDuringPresentation = false;
    
    dataServiceApi = [DataServiceAPI sharedManager];
    selectedContactData = [[NSDictionary alloc] init];
    tableData = [NSArray arrayWithArray:dataServiceApi.contacts];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return tableData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactTableViewCell" forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[ContactTableViewCell alloc] init];
    }
    
    [cell configureCell:tableData[indexPath.row]];
    
    return cell;
}


-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ContactTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    selectedContactData = cell.contactDataCopy;
    
    return indexPath;
}


- (void)updateSearchResultsForSearchController:(UISearchController *)searchController
{
    NSString *searchString = searchController.searchBar.text;
    
    if (searchString.length > 0) {
        //Filter results by name
        NSPredicate *predicate   = [NSPredicate predicateWithFormat:@"%K CONTAINS[cd] %@",@"name", searchString];
        tableData = [dataServiceApi.contacts filteredArrayUsingPredicate:predicate];
    } else {
        tableData = dataServiceApi.contacts;
    }
    
    [self.tableView reloadData];
}


/*
 // To be implemented in the future
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

#pragma mark - Scene Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ContactDetailTableViewController *newVC = [segue destinationViewController];
    newVC.contactDataCopy = selectedContactData;
    
    //This should be extecuted after the newVC appears
    self.searchController.searchBar.text = nil;
    [self.searchController setActive:NO];
}


@end
