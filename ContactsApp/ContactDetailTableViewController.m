//
//  ContactDetailTableViewController.m
//  ContactsApp
//
//  Created by Leonardo Amigoni on 2/13/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "ContactDetailTableViewController.h"
#import "DetailSimpleTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ContactDetailTableViewController ()

@end

@implementation ContactDetailTableViewController{
    NSMutableArray *tableData;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title =  self.contactDataCopy[@"name"];
    
    tableData = [NSMutableArray new];
    
    self.pictureImage.layer.cornerRadius = 10;
    self.pictureImage.clipsToBounds = YES;
    
    //Parse through data to make the array for the table
    //TODO: Create a proper model structure instead of getting data directly
    //from the Parsed Dictionary
    
    self.nameLabel.text = self.contactDataCopy[@"name"];
    self.positionLabel.text = self.contactDataCopy[@"position"];
    self.companyNameLabel.text = [NSString stringWithFormat:@"%@ - %@",self.contactDataCopy[@"companyDetails"][@"name"], self.contactDataCopy[@"companyDetails"][@"location"]];
    
    self.address1Label.text = [NSString stringWithFormat:@"%@ %@",self.contactDataCopy[@"address"][0][@"address1"], self.contactDataCopy[@"address"][0][@"address2"]];
     self.address2Label.text = [NSString stringWithFormat:@"%@ %@ %@",self.contactDataCopy[@"address"][0][@"city"], self.contactDataCopy[@"address"][0][@"state"], self.contactDataCopy[@"address"][0][@"zipcode"]];
    self.address3Label.text = [NSString stringWithFormat:@"%@",self.contactDataCopy[@"address"][0][@"country"]];
    
    [self.pictureImage sd_setImageWithURL:[NSURL URLWithString:self.contactDataCopy[@"imageUrl"]]
                     placeholderImage:[UIImage imageNamed:@"PlaceHolderSmall.png"]];
    
    if (self.contactDataCopy[@"phone"]) {
        for (NSDictionary* phone in self.contactDataCopy[@"phone"]) {
            NSMutableDictionary* cellData = [[NSMutableDictionary alloc] init];
            [cellData setValue:phone[@"type"] forKey:@"topLabel"];
            [cellData setValue:phone[@"number"] forKey:@"bottomLabel"];
            [tableData addObject:cellData];
        }
    }
    
    if (self.contactDataCopy[@"emails"]) {
        for (NSDictionary* email in self.contactDataCopy[@"emails"]) {
            NSMutableDictionary* cellData = [[NSMutableDictionary alloc] init];
            [cellData setValue:email[@"label"] forKey:@"topLabel"];
            [cellData setValue:email[@"email"] forKey:@"bottomLabel"];
            [tableData addObject:cellData];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return tableData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DetailSimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailSimpleTableViewCell" forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[DetailSimpleTableViewCell alloc] init];
    }
    
    NSMutableDictionary *rowData = tableData[indexPath.row];
    [cell configureCell: rowData[@"topLabel"] bottomLabelText:rowData[@"bottomLabel"]];
    
    return cell;
}


@end
