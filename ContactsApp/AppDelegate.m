//
//  AppDelegate.m
//  ContactsApp
//
//  Created by Leonardo Amigoni on 2/13/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "AppDelegate.h"
#import "DataServiceAPI.h"

@interface AppDelegate ()

@end

@implementation AppDelegate {
    DataServiceAPI *dataServiceAPI;
    UIViewController *startViewController;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    dataServiceAPI = [DataServiceAPI sharedManager];
    
    BOOL loggedIn =  [[NSUserDefaults standardUserDefaults] boolForKey:@"loggedIn"];
    
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    //If there is a user saved and he/she has logged in open the Contact List otherwise open authorization
    if (loggedIn == true && dataServiceAPI.uid) {
        startViewController = [storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    }
    else {
        startViewController = [storyboard instantiateViewControllerWithIdentifier:@"AuthorizeViewController"];
    }
    
    self.window.rootViewController = startViewController;
    [self.window makeKeyAndVisible];
    
    //Custumize Navbar
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:120.0/255 green:172.0/255 blue:207.0/255 alpha:1]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:145.0/255 green:145.0/255 blue:146.0/255 alpha:1]}];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
