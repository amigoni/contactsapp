//
//  ContactDetailSimpleTableViewCell.h
//  ContactsApp
//
//  Created by Leonardo Amigoni on 2/14/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailSimpleTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;

- (void) configureCell: (NSString *)topLabelText bottomLabelText:(NSString *) bottomLabelText;

@end
