//
//  ContactDetailSimpleTableViewCell.m
//  ContactsApp
//
//  Created by Leonardo Amigoni on 2/14/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import "DetailSimpleTableViewCell.h"

@implementation DetailSimpleTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void) configureCell: (NSString *)topLabelText bottomLabelText:(NSString *) bottomLabelText{
    self.topLabel.text = topLabelText;
    self.bottomLabel.text = bottomLabelText;
}


@end
