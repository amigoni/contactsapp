//
//  ContactTableViewCell.h
//  ContactsApp
//
//  Created by Leonardo Amigoni on 2/13/16.
//  Copyright © 2016 Mozzarello. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (copy, nonatomic) NSDictionary *contactDataCopy;

- (void) configureCell: (NSDictionary *)contactData;

@end
